import { useFavoriteDigimon } from "../../Providers/FavoriteDigimons";
import { IDigimon } from "../../types/digimon";
import Button from "../Button";
import { ContainerCard, Image } from "./styles";

interface IDigimonCardProps {
  digimon: IDigimon;
  isFavorite: boolean;
}

const DigimonCard = ({ digimon, isFavorite = false }: IDigimonCardProps) => {
  const { name, level, img } = digimon;

  const { addDigimon, deleteDigimon } = useFavoriteDigimon();
  console.log(isFavorite);
  return (
    <ContainerCard>
      <div>{name}</div>
      <Image src={img}></Image>
      <div>{level}</div>
      {isFavorite ? (
        <Button deleted={true} onClick={() => deleteDigimon(digimon)}>
          Remove
        </Button>
      ) : (
        <Button onClick={() => addDigimon(digimon)}>Add</Button>
      )}
    </ContainerCard>
  );
};

export default DigimonCard;
