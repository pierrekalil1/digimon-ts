import styled from "styled-components";

export const Container = styled.div`
  padding: 10px;
  margin: 10px;
  background-color: rgb(98, 98, 98);
  border: 0.5px solid #ddd;
  border-radius: 20px;
  font-size: 10px;
  color: rgb(231, 231, 231);
`;

export const ContainerCard = styled.div`
  width: 120px;
  padding: 10px;
  margin: 10px;
  background-color: rgb(98, 98, 98);
  border: 0.5px solid #ddd;
  border-radius: 20px;
  font-size: 10px;
  color: rgb(231, 231, 231);

  button {
    width: 50%;
  }
`;

export const Image = styled.img`
  width: 90px;
  height: 90px;
  border-radius: 30px;
  object-fit: cover;
`;
