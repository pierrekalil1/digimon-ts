import { createContext, useContext, useState, ReactNode } from "react";
import { IDigimon } from "../../types/digimon";

interface IFavoriteDigimonsProviderProps {
  children: ReactNode;
}

interface IFavoriteDigimonsProviderData {
  favorites: IDigimon[];
  addDigimon: (digimon: IDigimon) => void;
  deleteDigimon: (digimonToBeDeleted: IDigimon) => void;
}

const FavoriteDigimonsContext = createContext<IFavoriteDigimonsProviderData>(
  {} as IFavoriteDigimonsProviderData
);

export const FavoriteDigimonProvider = ({
  children,
}: IFavoriteDigimonsProviderProps) => {
  const [favorites, setFavorites] = useState<IDigimon[]>([] as IDigimon[]);

  const addDigimon = (digimon: IDigimon) => {
    setFavorites([...favorites, digimon]);
  };

  console.log(favorites);

  const deleteDigimon = (digimonToBeDeleted: IDigimon) => {
    const newList = favorites.filter(
      (digimon) => digimon.name !== digimonToBeDeleted.name
    );
    setFavorites(newList);
  };

  return (
    <FavoriteDigimonsContext.Provider
      value={{ favorites, addDigimon, deleteDigimon }}
    >
      {children}
    </FavoriteDigimonsContext.Provider>
  );
};

export const useFavoriteDigimon = () => useContext(FavoriteDigimonsContext);
