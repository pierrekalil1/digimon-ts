import { useEffect, useState } from "react";
import "./App.css";
import { Container } from "./components/CardDigimon/styles";
import Digimons from "./components/Digimons";
import { useFavoriteDigimon } from "./Providers/FavoriteDigimons";
import { ContainerApp, FavoritesList, List } from "./styles";

import { IDigimon } from "./types/digimon";

function App() {
  const { favorites } = useFavoriteDigimon();

  const [digimons, setDigimons] = useState<IDigimon[]>([] as IDigimon[]);

  const [error, setError] = useState<string>("");

  useEffect(() => {
    fetch("https://digimon-api.vercel.app/api/digimon")
      .then((response) => response.json())
      .then((response) => setDigimons([...response]))
      .catch(() => setError("Algo de errado!"));
  }, []);

  console.log(favorites);

  return (
    <div className="App">
      <header className="App-header">
        <h1>Escolha seu digimon</h1>
        <ContainerApp>
          <Container>
            <FavoritesList>
              <Digimons digimons={favorites} isFavorite={true} />
            </FavoritesList>
            <List>
              <Digimons digimons={digimons} />
            </List>
          </Container>
        </ContainerApp>
      </header>
    </div>
  );
}

export default App;
