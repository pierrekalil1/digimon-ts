import styled from "styled-components";

export const ContainerApp = styled.div`
  text-align: center;
  width: 80%;
`;

export const FavoritesList = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

export const List = styled.div`
  background-color: #282c34;
  min-height: 100vh;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  font-size: calc(10px + 2vmin);
  color: white;
`;
